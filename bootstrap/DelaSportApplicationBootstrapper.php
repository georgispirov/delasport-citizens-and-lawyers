<?php

namespace app\bootstrap;

use Yii;
use yii\base\BootstrapInterface;
use yii\base\Component;
use yii\base\Event;
use yii\web\Response;

class DelaSportApplicationBootstrapper extends Component implements BootstrapInterface
{
    const USER_ACCESSIBLE_NON_AUTHORIZED_URLS = [
        '/authentication/login', '/authentication/register', '/authentication/register-validation'
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param \yii\base\Application|\yii\web\Application $app the application currently running
     * @return \yii\console\Response|Response
     * @throws \yii\base\InvalidConfigException
     */
    public function bootstrap($app)
    {
        require_once __DIR__ . '/bootstrap.php';
        $this->setApplicationDefaultRoute($app);
        $this->setApplicationTimeZone();

        if (!$app->getUser()->isGuest) {
            return $this->setAppropriateEventActionDependsOnResponseStatusCode($app);
        }

        if (array_search($app->getRequest()->getUrl(), static::USER_ACCESSIBLE_NON_AUTHORIZED_URLS) === false) {
            return $this->redirectToHomeUrlNonAuthorizedUser($app);
        }
    }

    /**
     * @param \yii\web\Application $application
     */
    private function setApplicationDefaultRoute(\yii\web\Application $application)
    {
        if ($application->getUser()->isGuest) {
            $application->homeUrl = '/authentication/login';
        } else {
            $application->homeUrl = '/home/index';
        }

        $application->defaultRoute = $application->homeUrl;
    }

    /**
     * @param \yii\web\Application $application
     */
    private function setAppropriateEventActionDependsOnResponseStatusCode(\yii\web\Application $application)
    {
        $application->getResponse()->on(Response::EVENT_BEFORE_SEND, function (Event $responseEvent) use ($application) {
            /* @var $response Response */
            $response = $responseEvent->sender;

            switch ($response->statusCode) {
                case $response->getIsForbidden():
                    $application->errorHandler->errorAction = '/error/forbidden-access';
                    break;
                case $response->getIsNotFound():
                    $application->errorHandler->errorAction = '/error/not-found';
                    break;
                case $response->getIsServerError():
                    $application->errorHandler->errorAction = '/error/server-error';
                    break;
                default:
                    break;
            }

            if (!$application->request->isAjax
                && $application->getRequest()->getUrl() !== $application->errorHandler->errorAction
                && $response->statusCode != $response->getIsOk()
                && !$response->getIsRedirection())
            {
                return $response->redirect($application->errorHandler->errorAction);
            }
        });
    }

    /**
     * @param \yii\web\Application $application
     */
    private function redirectToHomeUrlNonAuthorizedUser(\yii\web\Application $application)
    {
        $application->getResponse()->on(Response::EVENT_BEFORE_SEND, function (Event $responseEvent) {
            /* @var $response Response */
            $response = $responseEvent->sender;
            return $response->redirect(Yii::$app->homeUrl);
        });
    }

    private function setApplicationTimeZone()
    {
        $dbTimeZone = Yii::$app->getDb()->getMasterPdo()->query('show timezone;')->fetchColumn(0);
        date_default_timezone_set($dbTimeZone);
        Yii::$app->getFormatter()->timeZone        = $dbTimeZone;
        Yii::$app->getFormatter()->defaultTimeZone = $dbTimeZone;
    }
}