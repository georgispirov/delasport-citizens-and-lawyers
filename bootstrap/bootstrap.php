<?php

use app\services\AuthenticationService;
use app\services\AuthenticationServiceInterface;
use app\services\CitizenAppointmentsService;
use app\services\CitizenAppointmentsServiceInterface;
use app\services\LawyerAppointmentsService;
use app\services\LawyerAppointmentsServiceInterface;
use app\services\ProfileService;
use app\services\ProfileServiceInterface;
use app\services\ScheduleService;
use app\services\ScheduleServiceInterface;

Yii::$container->set(AuthenticationServiceInterface::class, AuthenticationService::class);
Yii::$container->set(ProfileServiceInterface::class, ProfileService::class);
Yii::$container->set(CitizenAppointmentsServiceInterface::class, CitizenAppointmentsService::class);
Yii::$container->set(LawyerAppointmentsServiceInterface::class, LawyerAppointmentsService::class);
Yii::$container->set(ScheduleServiceInterface::class, ScheduleService::class);