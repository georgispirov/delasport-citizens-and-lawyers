<?php

namespace app\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\base\Model;

class PostgresTimestampDateParserAfterValidateBehavior extends Behavior
{
    /**
     * @var array $dates
     */
    public $dateAttributes;

    public function init()
    {
        if (empty($this->dateAttributes)) {
            throw new InvalidConfigException('Dates property cannot be empty.');
        }

        parent::init();
    }

    /**
     * @return array
     */
    public function events(): array
    {
        return [
            Model::EVENT_BEFORE_VALIDATE => 'parseDates'
        ];
    }

    /**
     * @param Event $event
     * @throws InvalidConfigException
     */
    public function parseDates(Event $event)
    {
        /* @var $model Model */
        $model = $event->sender;

        foreach ($this->dateAttributes as $attribute) {
            if (!empty($model->{$attribute})) {
                $model->{$attribute} = Yii::$app->getFormatter()->asDate($model->{$attribute}, Yii::$app->params['dbDateTimeFormat']);
            }
        }
    }
}