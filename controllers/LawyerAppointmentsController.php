<?php

namespace app\controllers;

use app\models\dto\ApproveOrRejectAppointmentForm;
use app\services\LawyerAppointmentsServiceInterface;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

class LawyerAppointmentsController extends Controller
{
    /**
     * @var LawyerAppointmentsServiceInterface $lawyerAppointmentsService
     */
    private $lawyerAppointmentsService;

    /**
     * LawyerAppointmentsController constructor.
     * @param string $id
     * @param Module $module
     * @param LawyerAppointmentsServiceInterface $lawyerAppointmentsService
     * @param array $config
     */
    public function __construct(string $id,
                                Module $module,
                                LawyerAppointmentsServiceInterface $lawyerAppointmentsService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->lawyerAppointmentsService = $lawyerAppointmentsService;
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'      => true,
                        'actions'    => ['approve-or-reject'],
                        'roles'      => ['approveAppointment', 'Lawyer'],
                        'roleParams' => function () {
                            return ['appointmentId' => Yii::$app->request->get('appointmentId')];
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $appointmentId
     * @return Response
     */
    public function actionApproveOrReject($appointmentId)
    {
        $model   = ApproveOrRejectAppointmentForm::findOne($appointmentId);
        $session = Yii::$app->getSession();

        if ($model->load($_POST) && $this->lawyerAppointmentsService->processAppointment($model)) {
            $actionMessageSuffix = $this->lawyerAppointmentsService->getSuffixSuccessfullyProcessedAppointment($model);
            $message             = 'Successfully ' . strtolower($model->action) . $actionMessageSuffix . ' citizen appointment';
            $session->setFlash('success', $message);
        } else {
            $session->setFlash('danger', 'Failed processing citizen appointment');
        }

        return $this->redirect(Url::to(['/user-appointments/index']));
    }
}