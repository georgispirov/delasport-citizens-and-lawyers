<?php

namespace app\controllers;

use app\models\searches\UsersSearch;
use app\models\UserType;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\web\Controller;

class LawyersController extends Controller
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'      => true,
                        'actions'    => ['index'],
                        'roles'      => ['@']
                    ],
                ],
            ]
        ];
    }

    /**
     * @return string
     * @throws InvalidConfigException
     */
    public function actionIndex()
    {
        $searchModel  = new UsersSearch;
        $dataProvider = $searchModel->search($_GET, UserType::LAWYER);
        return $this->render('/users/index', compact('searchModel', 'dataProvider'));
    }
}