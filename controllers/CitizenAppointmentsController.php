<?php

namespace app\controllers;

use app\models\Appointments;
use app\models\AppointmentStatus;
use app\models\dto\ApproveOrRejectAppointmentForm;
use app\models\dto\CitizenAppointmentForm;
use app\services\CitizenAppointmentsServiceInterface;
use http\Exception\InvalidArgumentException;
use HttpRequestMethodException;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\widgets\ActiveForm;

class CitizenAppointmentsController extends Controller
{
    /**
     * @var CitizenAppointmentsServiceInterface $citizenAppointmentsService
     */
    private $citizenAppointmentsService;

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'      => true,
                        'actions'    => ['view'],
                        'roles'      => ['previewAppointment'],
                        'roleParams' => function() {
                            return ['appointmentId' => Yii::$app->request->get('appointmentId')];
                        },
                    ],
                    [
                        'allow'      => true,
                        'actions'    => ['create', 'validation'],
                        'roles'      => ['Citizen']
                    ],
                    [
                        'allow'      => true,
                        'actions'    => ['update'],
                        'roles'      => ['updateAppointment', 'Citizen'],
                        'roleParams' => function() {
                            return ['appointmentId' => Yii::$app->request->get('appointmentId')];
                        },
                    ],
                    [
                        'allow'      => true,
                        'actions'    => ['delete'],
                        'roles'      => ['deleteAppointment'],
                        'roleParams' => function() {
                            return ['appointmentId' => Yii::$app->request->get('appointmentId')];
                        },
                    ]
                ],
            ],
        ];
    }

    /**
     * CitizenAppointmentsController constructor.
     * @param string $id
     * @param Module $module
     * @param CitizenAppointmentsServiceInterface $citizenAppointmentsService
     * @param array $config
     */
    public function __construct(string $id,
                                Module $module,
                                CitizenAppointmentsServiceInterface $citizenAppointmentsService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->citizenAppointmentsService = $citizenAppointmentsService;
    }

    public function actionView($appointmentId)
    {
        $model = ApproveOrRejectAppointmentForm::find()->joinWith(['userAppointment', 'status'])
                                                       ->where(['appointments.id' => $appointmentId])
                                                       ->one();

        if (Yii::$app->getUser()->identity->getIsLawyer() && $model->status_id === AppointmentStatus::STATUS_PENDING_REVIEW) {
            $this->citizenAppointmentsService->markAppointmentAsReceivedByLawyer($model);
        }

        return $this->render('view', compact('model'));
    }

    public function actionCreate()
    {
        $request = Yii::$app->getRequest();
        $model   = new CitizenAppointmentForm;

        if ($request->isGet) {
            return $this->render('form', compact('model'));
        }

        if ($model->load($request->post()) && $model->validate()) {
            $session = Yii::$app->getSession();

            if ($this->citizenAppointmentsService->saveCitizenAppointment($model)) {
                $session->setFlash('success', 'Successfully send your appointment');
            } else {
                $session->setFlash('danger', 'Failed sending your appointment.');
            }
        }

        return $this->redirect(['/user-appointments/index']);
    }

    public function actionUpdate($appointmentId)
    {
        $request = Yii::$app->getRequest();
        $model   = $this->citizenAppointmentsService->fetchAppointment($appointmentId);

        if ($request->isGet) {
            return $this->render('form', compact('model'));
        }

        if ($model->load($request->post())) {
            $session = Yii::$app->getSession();

            if ($this->citizenAppointmentsService->saveCitizenAppointment($model)) {
                $session->setFlash('success', 'Successfully updated your appointment');
            } else {
                $session->setFlash('danger', 'Failed updating your appointment.');
            }
        }

        return $this->redirect(['/user-appointments/index']);
    }

    public function actionDelete($appointmentId)
    {
        $request = Yii::$app->getRequest();

        if (!$request->isAjax || !$request->isPost) {
            throw new BadRequestHttpException('When deleting appointment, request method should be ajax post.');
        }

        $model = Appointments::findOne($appointmentId);

        if (!$model instanceof Appointments) {
            throw new InvalidArgumentException('Requested for deletion appointment cannot be found.');
        }

        $success = false;

        if ($this->citizenAppointmentsService->deleteAppointment($model)) {
            Yii::$app->getSession()->setFlash('success', 'Successfully deleted appointment.');
            $success = true;
        }

        return $this->asJson(compact('success'));
    }

    /**
     * @throws HttpRequestMethodException
     */
    public function actionValidation()
    {
        $request = Yii::$app->getRequest();

        if (!$request->isAjax && !$request->isPost) {
            throw new HttpRequestMethodException('Validation of appointment should be invoked asynchronously.');
        }

        $model = isset($request->post('CitizenAppointmentForm')['id']) ? CitizenAppointmentForm::findOne($request->post('CitizenAppointmentForm')['id'])
                                                                             : new CitizenAppointmentForm;

        $model->scenario = CitizenAppointmentForm::SCENARIO_VALIDATION;

        if (!$model->load($request->post())) {
            throw new InvalidArgumentException('Passed data cannot be applied successfully.');
        }

        $modelErrors = [];

        if (!empty($model->start_date) && !empty($model->end_date)
            && (!empty($model->lawyerId) || (!$model->isNewRecord && (int) $model->status_id === AppointmentStatus::STATUS_REJECTED)))
        {
            $lawyerAssignedAppointments = $this->citizenAppointmentsService->fetchLawyerApprovedAppointmentsByScheduledDate($model->lawyerId, $model->start_date, $model->end_date);

            if (!empty($lawyerAssignedAppointments)) {
                $modelErrors[Html::getInputId($model, 'lawyerId')] = ['Lawyer has already another approved appointments on this period.'];
            }

            if ($this->citizenAppointmentsService->hasRejectedAppointmentOverlappingDates($model)) {
                $modelErrors[Html::getInputId($model, 'start_date')] = ['Rejected appointment should not overlap previous start date.'];
                $modelErrors[Html::getInputId($model, 'end_date')]   = ['Rejected appointment should not overlap previous end date.'];
            }
        }

        return $this->asJson(array_merge(ActiveForm::validate($model), $modelErrors));
    }
}