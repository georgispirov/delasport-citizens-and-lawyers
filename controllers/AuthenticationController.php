<?php

namespace app\controllers;

use app\models\dto\LoginForm;
use app\models\dto\RegistrationForm;
use app\services\AuthenticationService;
use app\services\AuthenticationServiceInterface;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\IdentityInterface;
use yii\widgets\ActiveForm;

class AuthenticationController extends Controller
{
    public $layout = '//authentication';

    /**
     * @var AuthenticationServiceInterface $authenticationService
     */
    private $authenticationService;

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['login', 'register', 'register-validation'],
                        'roles'   => ['?'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['logout'],
                        'roles'   => ['@'],
                    ]
                ],
            ],
        ];
    }

    /**
     * AuthenticationController constructor.
     * @param string $id
     * @param Module $module
     * @param AuthenticationServiceInterface $authenticationService
     * @param array $config
     */
    public function __construct(string $id,
                                Module $module,
                                AuthenticationServiceInterface $authenticationService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->authenticationService = $authenticationService;
    }

    public function actionLogin()
    {
        $request = Yii::$app->getRequest();
        $model   = new LoginForm;

        if ($request->isGet) {
            return $this->render('login', compact('model'));
        }

        if (!$model->load($request->post()) || !$model->validate()) {
            return $this->redirect($request->getReferrer());
        }

        $session        = Yii::$app->getSession();
        $authentication = $this->authenticationService->login($model);

        if ($authentication === AuthenticationService::LOGIN_USER_CANNOT_BE_FOUND) {
            $session->setFlash('danger', 'User cannot be found.');
            return $this->redirect($request->getReferrer());
        }

        if ($authentication === AuthenticationService::LOGIN_PASSWORD_MISMATCHED) {
            $session->setFlash('danger', 'Password mismatched');
            return $this->redirect($request->getReferrer());
        }

        if ($authentication instanceof IdentityInterface) {
            Yii::$app->getUser()->login($authentication);
        }

        return $this->redirect('/home/index');
    }

    public function actionRegister()
    {
        $request = Yii::$app->getRequest();
        $model   = new RegistrationForm;

        if ($request->isGet) {
            return $this->render('register', compact('model'));
        }

        if ($model->load($request->post()) && $this->authenticationService->register($model)) {
            return $this->redirect('/home/index');
        }

        return $this->redirect($request->getReferrer());
    }

    public function actionRegisterValidation()
    {
        $request = Yii::$app->getRequest();

        if (!$request->isAjax || !$request->isPost) {
            throw new BadRequestHttpException('Request method of validating user should be invoked asynchronously.');
        }

        $model = new RegistrationForm;

        if ($model->load($request->post())) {
            return $this->asJson(ActiveForm::validate($model));
        }
    }

    public function actionLogout()
    {
        Yii::$app->getUser()->logout();
        return $this->redirect(Yii::$app->getUser()->loginUrl);
    }
}