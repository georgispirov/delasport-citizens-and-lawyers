<?php

namespace app\controllers;

use app\models\dto\UserProfile;
use app\services\ProfileServiceInterface;
use Firebase\JWT\JWT;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

class ProfileController extends Controller
{
    /**
     * @var ProfileServiceInterface $profileService
     */
    private $profileService;

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['personal-information', 'security', 'validation'],
                        'roles'   => ['@'],
                    ]
                ],
            ],
        ];
    }

    /**
     * ProfileController constructor.
     * @param string $id
     * @param Module $module
     * @param ProfileServiceInterface $profileService
     * @param array $config
     */
    public function __construct(string $id,
                                Module $module,
                                ProfileServiceInterface $profileService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->profileService = $profileService;
    }

    public function actionPersonalInformation($userId)
    {
        $request = Yii::$app->getRequest();

        if ($this->profileService->checkUserProfileIdentityAuthorization($request)) {
            Yii::$app->getSession()->setFlash('danger', 'You cannot access another user account.');
            return $this->redirect(['/profile/personal-information', 'userId' => JWT::urlsafeB64Encode(Yii::$app->getUser()->getId())]);
        }

        $user = $this->profileService->fetchUserProfile(JWT::urlsafeB64Decode($userId), UserProfile::SCENARIO_PERSONAL_INFORMATION);

        if ($request->isGet) {
            return $this->render('index', compact('user'));
        }

        if ($user->load($request->post())) {
            $session = Yii::$app->getSession();

            if ($user->save()) {
                $session->setFlash('success', 'Successfully saved profile personal information');
            } else {
                $session->setFlash('danger', 'Failed saving profile personal information');
            }
        }

        return $this->redirect(Url::to(['/profile/personal-information', 'userId' => $userId]));
    }

    public function actionSecurity($userId)
    {
        $request = Yii::$app->getRequest();

        if ($this->profileService->checkUserProfileIdentityAuthorization($request)) {
            Yii::$app->getSession()->setFlash('danger', 'You cannot access another user account.');
            return $this->redirect(['/profile/personal-information', 'userId' => JWT::urlsafeB64Encode(Yii::$app->getUser()->getId())]);
        }

        $user = $this->profileService->fetchUserProfile(JWT::urlsafeB64Decode($userId), UserProfile::SCENARIO_SECURITY);

        if ($request->isGet) {
            return $this->render('index', compact('user'));
        }

        $session = Yii::$app->getSession();

        if (!$user->load($request->post())) {
            $session->setFlash('danger', 'Invalid passed data.');
            return $this->redirect(Url::to(['/profile/security', 'userId' => $userId]));
        }

        $changePasswordProcess = $this->profileService->changeUserPassword($user);

        if ($changePasswordProcess === 'current-password-stored-password-mismatch') {
            $session->setFlash('danger', 'Current password mismatched with already stored one.');
        }

        if ($changePasswordProcess === 'current-password-confirm-password-mismatch') {
            $session->setFlash('danger', 'Current password and confirm password mismatched.');
        }

        if ($changePasswordProcess === true) {
            $session->setFlash('success', 'Successfully changed password');
        }

        return $this->redirect(Url::to(['/profile/security', 'userId' => $userId]));
    }

    /**
     * @param $userId
     * @return Response
     * @throws InvalidConfigException
     */
    public function actionValidation($userId)
    {
        $request = Yii::$app->getRequest();

        if ($this->profileService->checkUserProfileIdentityAuthorization($request)) {
            Yii::$app->getSession()->setFlash('danger', 'You cannot access another user account.');
            return $this->redirect(['/profile/personal-information', 'userId' => JWT::urlsafeB64Encode(Yii::$app->getUser()->getId())]);
        }

        if (!$request->isAjax || !$request->isPost) {
            throw new InvalidConfigException('Profile validation can be invoked only asynchronously and via post request.');
        }

        $model = new UserProfile(['scenario' => UserProfile::SCENARIO_SECURITY]);

        if (!$model->load($request->post())) {
            throw new InvalidArgumentException('Passed data cannot be load properly.');
        }

        if (empty($model->currentPassword)) {
            return $this->asJson(array_merge(ActiveForm::validate($model), [Html::getInputId($model, 'currentPassword') => ['Current Password cannot be blank.']]));
        }

        $hashedUserPassword = $this->profileService->fetchUserHashedPassword(JWT::urlsafeB64Decode($userId));

        if (!Yii::$app->getSecurity()->validatePassword($model->currentPassword, $hashedUserPassword)) {
            return $this->asJson(array_merge(ActiveForm::validate($model), [Html::getInputId($model, 'currentPassword') => ['Current password mismatched.']]));
        }

        return $this->asJson(ActiveForm::validate($model, ['password', 'confirmPassword']));
    }
}