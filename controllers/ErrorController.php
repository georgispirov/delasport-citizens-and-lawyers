<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class ErrorController extends Controller
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'      => true,
                        'actions'    => ['not-found', 'server-error', 'forbidden-access'],
                        'roles'      => ['@']
                    ],
                ],
            ]
        ];
    }

    /**
     * @param $action
     * @return bool|\yii\web\Response
     * @throws BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        $response = Yii::$app->getResponse();

        if ($response->getIsOk() && array_search($action->id, ['not-found', 'server-error', 'forbidden-access']) !== false) {
            return $this->redirect(Yii::$app->homeUrl);
        }

        return true;
    }

    public function actionNotFound()
    {
        Yii::$app->getResponse()->setStatusCode(404);
        return $this->render('not-found');
    }

    public function actionForbiddenAccess()
    {
        Yii::$app->getResponse()->setStatusCode(403);
        return $this->render('forbidden-access');
    }

    public function actionServerError()
    {
        Yii::$app->getResponse()->setStatusCode(500);
        return $this->render('server-error');
    }
}