<?php

namespace app\controllers;

use app\models\searches\UsersSearch;
use app\models\UserType;
use yii\filters\AccessControl;
use yii\web\Controller;

class CitizensController extends Controller
{
    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'      => true,
                        'actions'    => ['index'],
                        'roles'      => ['@']
                    ],
                ],
            ]
        ];
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        $searchModel  = new UsersSearch;
        $dataProvider = $searchModel->search($_GET, UserType::CITIZEN);
        return $this->render('/users/index', compact('searchModel', 'dataProvider'));
    }
}