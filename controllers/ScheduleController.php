<?php

namespace app\controllers;

use app\services\ScheduleServiceInterface;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\Controller;

class ScheduleController extends Controller
{
    /**
     * @var ScheduleServiceInterface $scheduleService
     */
    private $scheduleService;

    /**
     * ScheduleController constructor.
     * @param string $id
     * @param Module $module
     * @param ScheduleServiceInterface $scheduleService
     * @param array $config
     */
    public function __construct(string $id,
                                Module $module,
                                ScheduleServiceInterface $scheduleService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->scheduleService = $scheduleService;
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'fetch-schedule'],
                        'roles'   => ['@'],
                    ]
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return \yii\web\Response
     */
    public function actionFetchSchedule($startDate, $endDate)
    {
        return $this->asJson($this->scheduleService->fetchAppointments($startDate, $endDate));
    }
}