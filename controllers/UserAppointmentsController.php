<?php

namespace app\controllers;

use app\models\searches\UserAppointmentsSearch;
use yii\web\Controller;

class UserAppointmentsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel  = new UserAppointmentsSearch;
        $dataProvider = $searchModel->search($_GET);
        return $this->render('index', compact('searchModel', 'dataProvider'));
    }
}