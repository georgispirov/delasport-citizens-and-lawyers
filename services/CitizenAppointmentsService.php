<?php

namespace app\services;

use app\models\Appointments;
use app\models\AppointmentStatus;
use app\models\dto\ApproveOrRejectAppointmentForm;
use app\models\dto\CitizenAppointmentForm;
use app\models\UserAppointments;
use PDO;
use Throwable;
use Yii;
use yii\base\BaseObject;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Transaction;

class CitizenAppointmentsService extends BaseObject implements CitizenAppointmentsServiceInterface
{
    /**
     * @param $appointmentId
     * @return CitizenAppointmentForm|null
     */
    public function fetchAppointment($appointmentId): ?CitizenAppointmentForm
    {
        return CitizenAppointmentForm::find()->joinWith(['userAppointment'])
                                             ->where(['appointments.id' => $appointmentId])
                                             ->one();
    }

    /**
     * @param CitizenAppointmentForm $citizenAppointmentForm
     * @return bool
     */
    public function saveCitizenAppointment(CitizenAppointmentForm $citizenAppointmentForm): bool
    {
        $modelAttributes = array_intersect_key($citizenAppointmentForm->getAttributes(), array_flip($citizenAppointmentForm->activeAttributes()));

        if (!$this->hasCitizenChangedLawyerRecipient($citizenAppointmentForm)) {
            return $this->updateAppointmentAttributes($modelAttributes, $citizenAppointmentForm);
        }

        $transaction = Yii::$app->getDb()->beginTransaction();

        try {
            $this->saveAppointmentAttributes($modelAttributes);
            $this->saveUserAppointment($citizenAppointmentForm, $transaction);

            $transaction->commit();
            return true;
        } catch (Throwable $exception) {
            $transaction->rollBack();
            throw new $exception($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param CitizenAppointmentForm $citizenAppointmentForm
     * @return bool
     */
    private function hasCitizenChangedLawyerRecipient(CitizenAppointmentForm $citizenAppointmentForm): bool
    {
        return $citizenAppointmentForm->userAppointment->lawyer_id !== (int) $citizenAppointmentForm->lawyerId;
    }

    /**
     * @param CitizenAppointmentForm $citizenAppointmentForm
     * @return bool
     */
    private function hasUserAppointmentAlreadyCreated(CitizenAppointmentForm $citizenAppointmentForm): bool
    {
        $userAppointmentCompositePrimaryKeyAttributes = [
            'citizen_id'     => Yii::$app->getUser()->getId(),
            'lawyer_id'      => $citizenAppointmentForm->lawyerId,
            'appointment_id' => $citizenAppointmentForm->id
        ];

        return UserAppointments::find()->where($userAppointmentCompositePrimaryKeyAttributes)->exists();
    }

    /**
     * @param array $attributes
     * @return bool
     * @throws Exception
     */
    private function saveAppointmentAttributes(array $attributes): bool
    {
        $attributes = array_merge($attributes, ['status_id' => AppointmentStatus::STATUS_PENDING_REVIEW, 'created_date' => new Expression('now()')]);
        return Yii::$app->getDb()->createCommand()->insert(Appointments::tableName(), $attributes)->execute() > 0;
    }

    /**
     * @param array $attributes
     * @param CitizenAppointmentForm $citizenAppointmentForm
     * @return bool
     */
    private function updateAppointmentAttributes(array $attributes, CitizenAppointmentForm $citizenAppointmentForm): bool
    {
        if ((int) $citizenAppointmentForm->status_id === AppointmentStatus::STATUS_REJECTED && !$this->hasRejectedAppointmentOverlappingDates($citizenAppointmentForm)) {
            $attributes['status_id'] = AppointmentStatus::STATUS_RESCHEDULED;
        }

        return Appointments::updateAll(array_merge($attributes, ['updated_date' => new Expression('now()')]), ['id' => $citizenAppointmentForm->id]) > 0;
    }

    /**
     * @param CitizenAppointmentForm $citizenAppointmentForm
     * @param Transaction $transaction
     * @return bool
     * @throws Exception
     */
    private function saveUserAppointment(CitizenAppointmentForm $citizenAppointmentForm, Transaction $transaction): bool
    {
        if ($this->hasUserAppointmentAlreadyCreated($citizenAppointmentForm)) {
            $transaction->rollBack();
            return false;
        }

        $table = UserAppointments::tableName();

        $userAppointmentAttributes = [
            'appointment_id' => Yii::$app->getDb()->getLastInsertID('appointments_id_seq'),
            'lawyer_id'      => $citizenAppointmentForm->lawyerId,
            'citizen_id'     => Yii::$app->getUser()->getId()
        ];

        return Yii::$app->getDb()->createCommand()->insert($table, $userAppointmentAttributes)->execute() > 0;
    }

    /**
     * @param $lawyerId
     * @param $startDate
     * @param $endDate
     * @return array
     */
    public function fetchLawyerApprovedAppointmentsByScheduledDate($lawyerId, $startDate, $endDate): array
    {
        $sql = <<<SQL
        SELECT appointments.*
        FROM user_appointments
        LEFT JOIN appointments ON user_appointments.appointment_id = appointments.id
        WHERE (user_appointments.lawyer_id = :lawyer_id)
        AND (appointments.status_id = 2)
        AND (appointments.start_date::abstime::int <= :end_date) 
        AND (appointments.end_date::abstime::int >= :start_date)
SQL;

        $statement = Yii::$app->getDb()->getMasterPdo()->prepare($sql);
        $statement->bindParam(':lawyer_id', $lawyerId);
        $statement->bindParam(':start_date', strtotime($startDate));
        $statement->bindParam(':end_date', strtotime($endDate));
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param ApproveOrRejectAppointmentForm $appointmentForm
     * @return bool
     */
    public function markAppointmentAsReceivedByLawyer(ApproveOrRejectAppointmentForm $appointmentForm): bool
    {
        $appointmentForm->status_id = AppointmentStatus::STATUS_RECEIVED;
        return $appointmentForm->save();
    }

    /**
     * @param Appointments $appointment
     * @return bool
     */
    public function deleteAppointment(Appointments $appointment): bool
    {
        $transaction = Yii::$app->getDb()->beginTransaction();

        try {
            $this->deleteUserAppointment($appointment);
            Appointments::deleteAll(['id' => $appointment->id]);

            $transaction->commit();
            return true;
        } catch (Throwable $exception) {
            throw new $exception($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Appointments $appointment
     * @return bool
     */
    private function deleteUserAppointment(Appointments $appointment): bool
    {
        return UserAppointments::deleteAll(['appointment_id' => $appointment->id]) > 0;
    }

    /**
     * @param CitizenAppointmentForm $appointmentForm
     * @return bool
     */
    public function hasRejectedAppointmentOverlappingDates(CitizenAppointmentForm $appointmentForm): bool
    {
        $query = Appointments::find()->select([
            'to_char(appointments.start_date, \'MM/DD/YYYY HH24:MI\') as start_date',
            'to_char(appointments.end_date, \'MM/DD/YYYY HH24:MI\')   as end_date',
        ])->where('id = :appointment_id');

        $statement = Yii::$app->getDb()->getMasterPdo()->prepare($query->createCommand()->getRawSql());
        $statement->bindParam(':appointment_id', $appointmentForm->id);
        $statement->execute();

        list($rejectedAppointmentStartDate, $rejectedAppointmentEndDate) = $statement->fetch(PDO::FETCH_NUM);

        return $rejectedAppointmentEndDate >= $appointmentForm->start_date && $rejectedAppointmentStartDate <= $appointmentForm->end_date;
    }
}