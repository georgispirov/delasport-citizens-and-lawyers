<?php

namespace app\services;

use app\models\Appointments;
use app\models\dto\ApproveOrRejectAppointmentForm;
use app\models\dto\CitizenAppointmentForm;

interface CitizenAppointmentsServiceInterface
{
    /**
     * @param $appointmentId
     * @return CitizenAppointmentForm|null
     */
    public function fetchAppointment($appointmentId): ?CitizenAppointmentForm;

    /**
     * @param CitizenAppointmentForm $citizenAppointmentForm
     * @return bool
     */
    public function saveCitizenAppointment(CitizenAppointmentForm $citizenAppointmentForm): bool;

    /**
     * @param $lawyerId
     * @param $startDate
     * @param $endDate
     * @return array
     */
    public function fetchLawyerApprovedAppointmentsByScheduledDate($lawyerId, $startDate, $endDate): array;

    /**
     * @param ApproveOrRejectAppointmentForm $appointmentForm
     * @return bool
     */
    public function markAppointmentAsReceivedByLawyer(ApproveOrRejectAppointmentForm $appointmentForm): bool;

    /**
     * @param Appointments $appointment
     * @return bool
     */
    public function deleteAppointment(Appointments $appointment): bool;

    /**
     * @param CitizenAppointmentForm $appointmentForm
     * @return bool
     */
    public function hasRejectedAppointmentOverlappingDates(CitizenAppointmentForm $appointmentForm): bool;
}