<?php

namespace app\services;

use app\models\dto\UserProfile;
use app\models\Users;
use Firebase\JWT\JWT;
use Yii;
use yii\base\BaseObject;
use yii\base\Exception;
use yii\web\Request;

class ProfileService extends BaseObject implements ProfileServiceInterface
{
    /**
     * @param Request $request
     * @return bool
     */
    public function checkUserProfileIdentityAuthorization(Request $request): bool
    {
        $queryParams = $request->getQueryParams();
        return (int) JWT::urlsafeB64Decode($queryParams['userId']) !== Yii::$app->getUser()->getId();
    }

    /**
     * @param $userId
     * @param string $scenario
     * @return UserProfile|null
     */
    public function fetchUserProfile($userId, string $scenario = UserProfile::SCENARIO_PERSONAL_INFORMATION): ?UserProfile
    {
        $query = UserProfile::find();

        if ($scenario === UserProfile::SCENARIO_PERSONAL_INFORMATION) {
            $query->select(['users.id', 'users.first_name', 'users.last_name', 'users.username', 'users.email']);
        }

        if ($scenario === UserProfile::SCENARIO_SECURITY) {
            $query->select(['users.id']);
        }

        $user = $query->where(['users.id' => $userId])->one();

        if ($user instanceof UserProfile) {
            $user->scenario = $scenario;
        }

        return $user;
    }

    /**
     * @param $userId
     * @return string|null
     */
    public function fetchUserHashedPassword($userId): ?string
    {
        $sql = Users::find()->select(['users.password'])->where('users.id = :user_id');

        $statement = Yii::$app->getDb()->getMasterPdo()->prepare($sql->createCommand()->getRawSql());
        $statement->bindParam(':user_id', $userId);
        $statement->execute();

        return $statement->fetchColumn(0);
    }

    /**
     * @param UserProfile $profile
     * @return string|bool
     * @throws Exception
     */
    public function changeUserPassword(UserProfile $profile)
    {
        $hashedUserPassword = $this->fetchUserHashedPassword($profile->id);

        if (!Yii::$app->getSecurity()->validatePassword($profile->currentPassword, $hashedUserPassword)) {
            return 'current-password-stored-password-mismatch';
        }

        if ($profile->password !== $profile->confirmPassword) {
            return 'current-password-confirm-password-mismatch';
        }

        return Users::updateAll(['password' => Yii::$app->getSecurity()->generatePasswordHash($profile->password)]) > 0;
    }
}