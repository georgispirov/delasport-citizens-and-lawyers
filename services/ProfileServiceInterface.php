<?php

namespace app\services;

use app\models\dto\UserProfile;
use yii\web\Request;

interface ProfileServiceInterface
{
    /**
     * @param Request $request
     * @return bool
     */
    public function checkUserProfileIdentityAuthorization(Request $request): bool;
    
    /**
     * @param $userId
     * @param string $scenario
     * @return UserProfile|null
     */
    public function fetchUserProfile($userId, string $scenario): ?UserProfile;

    /**
     * @param $userId
     * @return string|null
     */
    public function fetchUserHashedPassword($userId): ?string;

    /**
     * @param UserProfile $profile
     * @return string|bool
     */
    public function changeUserPassword(UserProfile $profile);
}