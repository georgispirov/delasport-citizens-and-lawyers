<?php

namespace app\services;

use app\models\Appointments;
use app\models\AppointmentStatus;
use app\models\dto\ApproveOrRejectAppointmentForm;
use yii\base\BaseObject;

class LawyerAppointmentsService extends BaseObject implements LawyerAppointmentsServiceInterface
{
    /**
     * @param ApproveOrRejectAppointmentForm $appointmentForm
     * @return bool
     */
    public function processAppointment(ApproveOrRejectAppointmentForm $appointmentForm): bool
    {
        $appointmentForm->status_id = $appointmentForm->action === 'Approve' ? AppointmentStatus::STATUS_APPROVED : AppointmentStatus::STATUS_REJECTED;
        return $appointmentForm->save();
    }

    /**
     * @param ApproveOrRejectAppointmentForm $appointmentForm
     * @return string
     */
    public function getSuffixSuccessfullyProcessedAppointment(ApproveOrRejectAppointmentForm $appointmentForm): string
    {
        return $appointmentForm->action === 'Approve' ? 'd' : 'ed';
    }
}