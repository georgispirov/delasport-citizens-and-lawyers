<?php

namespace app\services;

use Generator;

interface ScheduleServiceInterface
{
    /**
     * @param $startDate
     * @param $endDate
     * @return Generator
     */
    public function fetchAppointments($startDate, $endDate): Generator;
}