<?php

namespace app\services;

use app\models\dto\LoginForm;
use app\models\dto\RegistrationForm;
use app\models\Users;
use app\models\UserType;
use Yii;
use yii\base\BaseObject;
use yii\web\IdentityInterface;

class AuthenticationService extends BaseObject implements AuthenticationServiceInterface
{
    const LOGIN_USER_CANNOT_BE_FOUND = 'user-cannot-be-found';
    const LOGIN_PASSWORD_MISMATCHED  = 'password-mismatched';
    const LOGIN_USER_FOUND           = 'user-found';

    /**
     * @param RegistrationForm $registrationForm
     * @return mixed|string
     * @throws \Exception
     */
    public function register(RegistrationForm $registrationForm): bool
    {
        if ($registrationForm->password !== $registrationForm->confirmPassword) {
            return 'confirm-password-mismatched';
        }

        $identity = new Users($registrationForm->getAttributes(null, ['id']));
        $identity->password = Yii::$app->getSecurity()->generatePasswordHash($identity->password);

        if (!$identity->save()) {
            return false;
        }

        $authManager = Yii::$app->getAuthManager();
        $roleType    = $identity->type_id === UserType::CITIZEN ? 'Citizen' : 'Lawyer';

        $authManager->assign($authManager->getRole($roleType), $identity->id);
        Yii::$app->getUser()->login($identity);

        return true;
    }

    /**
     * @param LoginForm $loginForm
     * @return Users|array|string|null
     */
    public function login(LoginForm $loginForm)
    {
        $user = Users::find()->where([
            'or',
            ['users.username' => $loginForm->identifier],
            ['users.email'    => $loginForm->identifier]
        ])->one();

        if (!$user instanceof IdentityInterface) {
            return 'user-cannot-be-found';
        }

        if (!Yii::$app->getSecurity()->validatePassword($loginForm->password, $user->password)) {
            return 'password-mismatched';
        }

        return $user;
    }
}