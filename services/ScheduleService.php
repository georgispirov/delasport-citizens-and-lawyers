<?php

namespace app\services;

use app\models\Appointments;
use app\models\UserAppointmentsQuery;
use app\models\Users;
use app\models\UsersQuery;
use Generator;
use PDO;
use Yii;
use yii\base\BaseObject;

class ScheduleService extends BaseObject implements ScheduleServiceInterface
{
    /**
     * @param $startDate
     * @param $endDate
     * @return Generator
     */
    public function fetchAppointments($startDate, $endDate): Generator
    {
        $query = Appointments::find()->select([
            'appointments.id', 'appointments.name', 'appointments.description',
            'to_char(appointments.start_date, \'YYYY-MM-DD HH24:MI\') as start_date',
            'to_char(appointments.end_date, \'YYYY-MM-DD HH24:MI\')   as end_date',
            "concat_ws(' ', lawyer.first_name, lawyer.last_name)   as lawyerfullnames",
            "concat_ws(' ', citizen.first_name, citizen.last_name) as citizenfullnames",
            'appointment_status.name as status'
        ]);

        $query->joinWith([
            'userAppointment' => function (UserAppointmentsQuery $userAppointmentsQuery) {
                $userAppointmentsQuery->joinWith([
                    'lawyer' => function (UsersQuery $lawyerQuery) {
                        $lawyerQuery->from(['lawyer' => Users::tableName()]);
                        return $lawyerQuery;
                    },
                    'citizen' => function (UsersQuery $citizenQuery) {
                        $citizenQuery->from(['citizen' => Users::tableName()]);
                        return $citizenQuery;
                    }
                ]);

                return $userAppointmentsQuery;
            },
            'status'
        ]);

        $query->andWhere([
            'and',
            'appointments.start_date >= :start_date',
            'appointments.end_date   <= :end_date',
        ]);

        $statement = Yii::$app->getDb()->getMasterPdo()->prepare($query->createCommand()->getRawSql());
        $statement->bindParam(':start_date', $startDate);
        $statement->bindParam(':end_date', $endDate);
        $statement->execute();

        while ($appointment = $statement->fetch(PDO::FETCH_ASSOC)) {
            yield $appointment;
        }
    }
}