<?php

namespace app\services;

use app\models\dto\ApproveOrRejectAppointmentForm;

interface LawyerAppointmentsServiceInterface
{
    /**
     * @param ApproveOrRejectAppointmentForm $appointmentForm
     * @return bool
     */
    public function processAppointment(ApproveOrRejectAppointmentForm $appointmentForm): bool;

    /**
     * @param ApproveOrRejectAppointmentForm $appointmentForm
     * @return string
     */
    public function getSuffixSuccessfullyProcessedAppointment(ApproveOrRejectAppointmentForm $appointmentForm): string;
}