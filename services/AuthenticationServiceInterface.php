<?php

namespace app\services;

use app\models\dto\LoginForm;
use app\models\dto\RegistrationForm;
use yii\web\IdentityInterface;

interface AuthenticationServiceInterface
{
    /**
     * @param RegistrationForm $registrationForm
     * @return bool
     */
    public function register(RegistrationForm $registrationForm): bool;

    /**
     * @param LoginForm $loginForm
     * @return string|IdentityInterface
     */
    public function login(LoginForm $loginForm);
}