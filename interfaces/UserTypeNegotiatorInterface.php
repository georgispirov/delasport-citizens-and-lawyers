<?php

namespace app\interfaces;

interface UserTypeNegotiatorInterface
{
    /**
     * @return bool
     */
    public function getIsCitizen(): bool;

    /**
     * @return bool
     */
    public function getIsLawyer(): bool;
}