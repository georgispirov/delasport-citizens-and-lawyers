<?php

namespace app\assets;

use yii\web\AssetBundle;

class FullCalendarAsset extends AssetBundle
{
    public $js = [
        '//cdn.jsdelivr.net/npm/fullcalendar@3.10.1/dist/fullcalendar.min.js',
        '//cdn.jsdelivr.net/npm/fullcalendar@3.10.1/dist/locale-all.js'
    ];

    public $css = [
        '//cdn.jsdelivr.net/npm/fullcalendar@3.10.1/dist/fullcalendar.css'
    ];
}