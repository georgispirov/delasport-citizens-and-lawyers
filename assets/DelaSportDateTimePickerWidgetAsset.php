<?php

namespace app\assets;

use yii\web\AssetBundle;

class DelaSportDateTimePickerWidgetAsset extends AssetBundle
{
    public $css = ['//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css'];

    public $js  = [
        '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js',
        'js/dela-sport-date-time-picker.js'
    ];
}