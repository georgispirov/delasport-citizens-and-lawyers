<?php

namespace app\assets;

use rmrevin\yii\fontawesome\CdnFreeAssetBundle;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\web\YiiAsset;

class MainApplicationAsset extends AssetBundle
{
    public $jsOptions  = ['position' => View::POS_HEAD];
    public $cssOptions = ['position' => View::POS_HEAD];

    public $depends = [
        BootstrapAsset::class,
        JqueryAsset::class,
        YiiAsset::class,
        CdnFreeAssetBundle::class,
        DelaSportDateTimePickerWidgetAsset::class,
        FullCalendarAsset::class,
        DelaSportSweetAlertAsset::class
    ];
}