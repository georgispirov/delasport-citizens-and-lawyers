<?php

namespace app\assets;

use yii\web\AssetBundle;

class DelaSportSweetAlertAsset extends AssetBundle
{
    public $js  = ['//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js'];
    public $css = ['//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css'];
}