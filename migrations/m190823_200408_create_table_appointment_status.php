<?php

use yii\db\Migration;

class m190823_200408_create_table_appointment_status extends Migration
{
    public function safeUp()
    {
        $this->createTable('appointment_status', [
            'id'   => $this->primaryKey(),
            'name' => $this->string(200)
        ]);

        foreach (['Pending-Review', 'Approved', 'Rejected', 'Received', 'Rescheduled'] as $type) {
            $this->insert('appointment_status', [
                'name' => $type
            ]);
        }

        $this->addColumn('appointments', 'status_id', $this->integer());

        $this->addForeignKey(
            'fk_appointments_status_id',
            'appointments',
            'status_id',
            'appointment_status',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
        echo "m190823_200408_create_table_appointment_status cannot be reverted.\n";
        return true;
    }
}
