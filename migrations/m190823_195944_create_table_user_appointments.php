<?php

use yii\db\Migration;

class m190823_195944_create_table_user_appointments extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_appointments', [
            'citizen_id'     => $this->integer(),
            'lawyer_id'      => $this->integer(),
            'appointment_id' => $this->integer()
        ]);

        $this->addPrimaryKey('pk_user_appointments', 'user_appointments', ['citizen_id', 'lawyer_id', 'appointment_id']);

        $this->addForeignKey(
            'fk_user_appointments_citizen_id',
            'user_appointments',
            'citizen_id',
            'users',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk_user_appointments_lawyer_id',
            'user_appointments',
            'lawyer_id',
            'users',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        $this->addForeignKey(
            'fk_user_appointments_appointment_id',
            'user_appointments',
            'appointment_id',
            'appointments',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
        echo "m190823_195944_create_table_user_appointments cannot be reverted.\n";
        return true;
    }
}
