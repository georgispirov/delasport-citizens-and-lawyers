<?php

use yii\db\Migration;

class m190823_133152_create_table_user_type extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_type', ['id' => $this->primaryKey(), 'name' => $this->string(155)]);

        foreach (['Citizen', 'Lawyer'] as $type) {
            $this->insert('user_type', [
                'name' => $type
            ]);
        }

        $this->addColumn('users', 'type_id', $this->integer()->defaultValue(null));

        $this->addForeignKey('fk_user_type', 'users', 'type_id', 'user_type', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function safeDown()
    {
        echo "m190823_133152_create_table_user_type cannot be reverted.\n";
        return true;
    }
}
