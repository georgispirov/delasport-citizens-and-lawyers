<?php

use yii\db\Migration;

class m190823_063346_create_table_users extends Migration
{
    public function safeUp()
    {
        $this->createTable('users', [
            'id'           => $this->primaryKey(),
            'first_name'   => $this->string(155),
            'last_name'    => $this->string(155),
            'username'     => $this->string(200),
            'email'        => $this->string(),
            'password'     => $this->string(),
            'created_date' => $this->timestamp()->append('with time zone default now()'),
            'updated_date' => $this->timestamp()->append('with time zone')
        ]);
    }

    public function safeDown()
    {
        echo "m190823_063346_create_table_users cannot be reverted.\n";
        return true;
    }
}
