<?php

use yii\db\Migration;

class m190823_195401_create_table_appointments extends Migration
{
    public function safeUp()
    {
        $this->createTable('appointments', [
            'id'           => $this->primaryKey(),
            'name'         => $this->string(),
            'description'  => $this->text(),
            'created_date' => $this->timestamp()->append('with time zone default now()'),
            'updated_date' => $this->timestamp()->append('with time zone'),
            'start_date'   => $this->timestamp()->append('with time zone'),
            'end_date'     => $this->timestamp()->append('with time zone')
        ]);
    }

    public function safeDown()
    {
        echo "m190823_195401_create_table_appointments cannot be reverted.\n";
        return true;
    }
}
