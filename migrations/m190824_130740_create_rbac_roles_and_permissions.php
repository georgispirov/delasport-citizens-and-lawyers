<?php

use app\auth_manager_rules\ApproveAppointmentRule;
use app\auth_manager_rules\DeleteAppointmentRule;
use app\auth_manager_rules\PreviewAppointmentRule;
use app\auth_manager_rules\RejectAppointmentRule;
use app\auth_manager_rules\UpdateAppointmentRule;
use yii\db\Migration;
use yii\rbac\Permission;

class m190824_130740_create_rbac_roles_and_permissions extends Migration
{
    const USER_ROLES          = ['Citizen', 'Lawyer'];

    const CITIZEN_PERMISSIONS = [
        'createAppointment', 'previewAppointment', 'updateAppointment', 'deleteAppointment',
    ];

    const LAWYER_PERMISSIONS  = [
        'previewAppointment', 'approveAppointment', 'rejectAppointment', 'deleteAppointment'
    ];

    const RULES = [
        'approveAppointment'    => ApproveAppointmentRule::class,
        'deleteAppointment'     => DeleteAppointmentRule::class,
        'previewAppointment'    => PreviewAppointmentRule::class,
        'rejectAppointment'     => RejectAppointmentRule::class,
        'updateAppointment'     => UpdateAppointmentRule::class,
    ];

    /**
     * @return bool|void
     * @throws Exception
     */
    public function safeUp()
    {
        $authManager = Yii::$app->getAuthManager();

        foreach (static::USER_ROLES as $role) {
            if ($role === 'Citizen') {
                $citizenRole = $authManager->createRole($role);
                $authManager->add($citizenRole);

                foreach (static::CITIZEN_PERMISSIONS as $citizenPermission) {
                    $permission = $authManager->createPermission($citizenPermission);
                    $authManager->add($permission);
                    $authManager->addChild($citizenRole, $permission);
                }
            } else {
                $lawyerRole = $authManager->createRole($role);
                $authManager->add($lawyerRole);

                foreach (static::LAWYER_PERMISSIONS as $citizenPermission) {
                    $permission = $authManager->getPermission($citizenPermission);

                    if (!$permission instanceof Permission) {
                        $permission = $authManager->createPermission($citizenPermission);
                        $authManager->add($permission);
                    }

                    $authManager->addChild($lawyerRole, $permission);
                }
            }
        }

        foreach (static::RULES as $permissionName => $rule) {
            $permission = $authManager->getPermission($permissionName);
            $permission->ruleName = $rule;
            $authManager->update($permissionName, $permission);
        }
    }

    public function safeDown()
    {
        echo "m190824_130740_create_rbac_roles_and_permissions cannot be reverted.\n";
        return true;
    }
}
