<?php

/* @var $this View */

use yii\web\View; ?>

<h3>Lawyers / Citizens Appointments Schedule</h3>

<div id="schedule"></div>

<script>
    $(function () {
        $('#schedule').fullCalendar({
            header: {
                left:   'title',
                center: '',
                right:  'today prev,next'
            },
            events: function (start, end, timezone, callback) {
                $.get('/schedule/fetch-schedule?startDate=' + new Date(start).toISOString(), '&endDate=' + new Date(end).toISOString(), function (response) {
                    let appointments = [];

                    $.map(response, function (appointment) {
                        appointments.push({
                            id          : appointment.id,
                            title       : appointment.name,
                            description : appointment.description,
                            start       : appointment.start_date,
                            end         : appointment.end_date,
                            lawyer      : appointment.lawyerfullnames,
                            citizen     : appointment.citizenfullnames,
                            status      : appointment.status
                        });
                    });

                    callback(appointments);
                });
            },
            eventRender: function(event, element) {
                element.popover({
                    container: 'body',
                    html: true,
                    trigger: 'hover',
                    placement: "bottom",
                    content: '<div class="popover" role="tooltip">' +
                                '<div class="arrow"></div>' +
                                '<h3 class="popover-header">' + event.title + '</h3>' +
                                '<div class="popover-body">' +
                                    '<div class="row">' +
                                        '<b>Start Date: </b>' + event.start._i +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<b>End Date: </b>' +
                                        '<span>' + event.end._i + '</span>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<b>Description: </b>' +
                                        '<span>' + event.description + '</span>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<b>Lawyer: </b>' +
                                        '<span>' + event.lawyer + '</span>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<b>Citizen: </b>' +
                                        '<span>' + event.citizen + '</span>' +
                                    '</div>' +
                                    '<div class="row">' +
                                        '<b>Status: </b>' +
                                        '<span>' + event.status + '</span>' +
                                    '</div>' +
                                '</div>' +
                            '</div>',
                });
            },
            eventClick: function(info) {
                location.href = '/citizen-appointments/view?appointmentId=' + info.id;
            }
        });
    });
</script>

<style>
    .popover {
        width:250px !important;
        height:250px !important;
        display: block;
    }

    .row {
        margin: 0 !important;
    }
</style>