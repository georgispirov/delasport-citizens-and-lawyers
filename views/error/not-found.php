<?php

/* @var $this View */

use yii\web\View; ?>

<div class="alert alert-danger">
    The page you are looking for does not exists.
</div>
