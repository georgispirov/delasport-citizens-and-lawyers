<?php

/* @var $this View */

use yii\web\View; ?>

<div class="alert alert-danger">
    You don't have permission to access this page.
</div>
