<?php

/* @var $this View */
/* @var $content string */

use app\assets\MainApplicationAsset;
use yii\bootstrap\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\web\View;

MainApplicationAsset::register($this);

$this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php
    $this->beginBody(); ?>

    <div class="wrap">
        <?php

        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl'   => Yii::$app->homeUrl,
            'options'    => [
                'class' => 'navbar-inverse',
            ],
        ]);

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items'   => [
                ['label' => 'Login',    'url' => ['/authentication/login']],
                ['label' => 'Register', 'url' => ['/authentication/register']]
            ]
        ]);

        NavBar::end();
        ?>
    </div>

    <div class="container">
    <?php
        foreach (Yii::$app->getSession()->getAllFlashes() as $type => $message) {
            echo Alert::widget([
                'body'    => $message,
                'options' => ['class' => "alert alert-{$type}"]
            ]);
        }

        echo $content;
        $this->endBody();
    ?>
    </div>
</body>
</html>
<?php $this->endPage() ?>
