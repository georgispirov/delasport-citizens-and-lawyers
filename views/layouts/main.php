<?php

/* @var $this View */
/* @var $content string */

use app\assets\MainApplicationAsset;
use Firebase\JWT\JWT;
use yii\bootstrap\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Breadcrumbs;

$this->registerCssFile('//cdn.jsdelivr.net/npm/fullcalendar@3.10.1/dist/fullcalendar.print.min.css', ['media' => 'print', 'position' => View::POS_HEAD]);

MainApplicationAsset::register($this);

$this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
<?php
    $this->beginBody(); ?>

    <div class="wrap">
        <?php

        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl'   => Yii::$app->homeUrl,
            'options'    => [
                'class' => 'navbar-inverse',
            ],
        ]);

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items'   => [
                [
                    'label' => 'Lawyers',
                    'url'   => ['/lawyers']
                ],
                [
                    'label' => 'Citizens',
                    'url'   => ['/citizens']
                ],
                [
                    'label' => 'Schedule',
                    'url'   => ['/schedule']
                ],
                [
                    'label' => 'Appointments',
                    'url'   => ['/user-appointments/index']
                ],
                [
                    'label' => 'Profile (' . (Yii::$app->getUser()->identity->getIsCitizen() ? 'Citizen' : 'Lawyer') . ')',
                    'url'   => ['/profile/personal-information', 'userId' => JWT::urlsafeB64Encode(Yii::$app->getUser()->identity->getId())]
                ],
                '<li>' . Html::a('Logout (' . Yii::$app->user->identity->username . ')', Url::to(['/authentication/logout']), ['class' => 'btn btn-link logout']) . '</li>'
            ]
        ]);

        NavBar::end();
        ?>
    </div>

    <div class="container">
    <?php
        foreach (Yii::$app->getSession()->getAllFlashes(true) as $type => $message) {
            echo Alert::widget([
                'body'    => $message,
                'options' => ['class' => "alert alert-{$type}"]
            ]);
        }

        echo Breadcrumbs::widget([
            'encodeLabels' => false,
            'links'        => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);

        echo $content;
        $this->endBody();
    ?>
    </div>
</body>

</html>
<?php $this->endPage() ?>
