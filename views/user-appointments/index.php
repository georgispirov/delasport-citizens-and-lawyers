<?php

/* @var $this View */
/* @var $searchModel UserAppointmentsSearch */
/* @var $dataProvider ActiveDataProvider */

use app\models\searches\UserAppointmentsSearch;
use rmrevin\yii\fontawesome\FAS;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

echo $this->render('_search', compact('searchModel'));

if (Yii::$app->getUser()->identity->getIsCitizen()) {
    echo Html::a(FAS::icon(FAS::_PLUS), Url::to(['/citizen-appointments/create']), [
        'data-toggle' => 'tooltip',
        'title'       => 'Add Appointment'
    ]);
}

echo $this->render('_grid', compact('dataProvider')); ?>

<script>
    $(function () {
        $('.js-delete-appointment').on('click', function () {
            let $button = $(this);
            
            $.post($button.data('url'), function (response) {
                if (response.success) {
                    location.reload();
                    return;
                }

                Swal.fire(
                    'Failed deleting appointment.',
                    'error'
                );
            });
        });
    });
</script>
