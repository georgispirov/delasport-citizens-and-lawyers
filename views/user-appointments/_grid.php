<?php

/* @var $this View */
/* @var $dataProvider ActiveDataProvider */

use app\models\UserAppointments;
use rmrevin\yii\fontawesome\FAS;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View; ?>

<div class="row">
    <div class="col-xs-12">
        <?php
            if (Yii::$app->getUser()->identity->getIsCitizen()) {
                echo Html::tag('h3', 'My Appointments');
            } else {
                echo Html::tag('h3', 'Requested Appointments');
            }
        ?>
        <?= GridView::widget([
            'options'      => ['class' => 'table-responsive grid-view'],
            'dataProvider' => $dataProvider,
            'columns'      => [
                [
                    'attribute' => 'citizen',
                    'value'     => function (UserAppointments $userAppointment) {
                        return $userAppointment->citizen->first_name . ' ' . $userAppointment->citizen->last_name;
                    }
                ],
                [
                    'attribute' => 'lawyer',
                    'value'     => function (UserAppointments $userAppointment) {
                        return $userAppointment->lawyer->first_name . ' ' . $userAppointment->lawyer->last_name;
                    }
                ],
                'appointment.name',
                'appointment.description',
                [
                    'attribute' => 'appointment.status.name',
                    'label'     => 'Status'
                ],
                'appointment.start_date',
                'appointment.end_date',
                'appointment.created_date',
                'appointment.updated_date',
                [
                    'class'          => ActionColumn::class,
                    'template'       => '{view} {update} {delete}',
                    'visibleButtons' => [
                        'view'   => function (UserAppointments $userAppointment) {
                            return Yii::$app->user->can('previewAppointment', ['appointmentId' => $userAppointment->appointment_id]);
                        },
                        'update' => function (UserAppointments $userAppointment) {
                            return Yii::$app->getUser()->can('updateAppointment', ['appointmentId' => $userAppointment->appointment_id]);
                        },
                        'delete' => function (UserAppointments $userAppointment) {
                            return Yii::$app->getUser()->can('deleteAppointment', ['appointmentId' => $userAppointment->appointment_id]);
                        }
                    ],
                    'buttons'  => [
                        'view'   => function ($url, UserAppointments $userAppointment) {
                            return Html::a(FAS::icon(FAS::_EYE), Url::to(['/citizen-appointments/view', 'appointmentId' => $userAppointment->appointment_id]));
                        },
                        'update' => function ($url, UserAppointments $userAppointment) {
                            return Html::a(FAS::icon(FAS::_PENCIL_ALT), Url::to(['/citizen-appointments/update', 'appointmentId' => $userAppointment->appointment_id]));
                        },
                        'delete' => function ($url, UserAppointments $userAppointment) {
                            return Html::button(FAS::icon(FAS::_TRASH_ALT), [
                                'class'    => 'js-delete-appointment',
                                'data-url' => Url::to(['/citizen-appointments/delete', 'appointmentId' => $userAppointment->appointment_id])
                            ]);
                        }
                    ]
                ]
            ]
        ]); ?>
    </div>
</div>
