<?php

/* @var $this View */
/* @var $searchModel UserAppointmentsSearch */

use app\models\AppointmentStatus;
use app\models\searches\UserAppointmentsSearch;
use app\widgets\DelaSportDateTimePickerWidget;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'method' => 'GET',
    'action' => Url::to(["/user-appointments"])
]);

echo $form->field($searchModel, 'commonFilter')->textInput([
    'placeholder' => $searchModel->getAttributeLabel('commonFilter')
])->label(false);

if (!empty($searchModel->startDateRange) && !empty($searchModel->endDateRange)) {
    $searchModel->startDateRange = Yii::$app->getFormatter()->asDatetime($searchModel->startDateRange, 'MM/dd/Y HH:i');
    $searchModel->endDateRange   = Yii::$app->getFormatter()->asDatetime($searchModel->endDateRange, 'MM/dd/Y HH:i');
}

if (!empty($searchModel->startCreatedDateRange) && !empty($searchModel->endCreatedDateRange)) {
    $searchModel->startCreatedDateRange = Yii::$app->getFormatter()->asDatetime($searchModel->startCreatedDateRange, 'MM/dd/Y HH:i');
    $searchModel->endCreatedDateRange   = Yii::$app->getFormatter()->asDatetime($searchModel->endCreatedDateRange, 'MM/dd/Y HH:i');
}

if (!empty($searchModel->startUpdatedDateRange) && !empty($searchModel->endUpdatedDateRange)) {
    $searchModel->startUpdatedDateRange = Yii::$app->getFormatter()->asDatetime($searchModel->startUpdatedDateRange, 'MM/dd/Y HH:i');
    $searchModel->endUpdatedDateRange   = Yii::$app->getFormatter()->asDatetime($searchModel->endUpdatedDateRange, 'MM/dd/Y HH:i');
}

echo Html::button(Html::a('Reset Filters', Url::to(["/user-appointments"])));

Modal::begin([
    'toggleButton' => [
        'tag'   => 'button',
        'label' => 'Additional Filters',
        'class' => 'btn btn-success'
    ]
]); ?>

<?= $form->field($searchModel, 'startDateRange')->widget(DelaSportDateTimePickerWidget::class); ?>

<?= $form->field($searchModel, 'endDateRange')->widget(DelaSportDateTimePickerWidget::class); ?>

<hr/>

<?= $form->field($searchModel, 'startCreatedDateRange')->widget(DelaSportDateTimePickerWidget::class); ?>

<?= $form->field($searchModel, 'endCreatedDateRange')->widget(DelaSportDateTimePickerWidget::class); ?>

<hr/>

<?= $form->field($searchModel, 'startUpdatedDateRange')->widget(DelaSportDateTimePickerWidget::class); ?>

<?= $form->field($searchModel, 'endUpdatedDateRange')->widget(DelaSportDateTimePickerWidget::class); ?>

<hr/>

<?= $form->field($searchModel, 'appointmentStatusId')->dropDownList(ArrayHelper::map(AppointmentStatus::find()->all(), 'id', 'name'), [
    'prompt' => 'Filter By Status'
]); ?>

<?php

echo Html::submitButton('Search', ['class' => 'btn btn-success']);

Modal::end();

ActiveForm::end();