<?php

/* @var $this View */
/* @var $model LoginForm */

use app\models\dto\LoginForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm; ?>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'login-form'],
    'action'  => Url::to(['/authentication/login']),
    'method'  => 'POST'
]); ?>

<?= $form->field($model, 'identifier')->textInput(); ?>

<?= $form->field($model, 'password')->passwordInput(); ?>

<?= Html::submitButton('Login', ['class' => 'btn btn-success']); ?>

<?php ActiveForm::end(); ?>