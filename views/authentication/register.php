<?php

/* @var $this View */
/* @var $model RegistrationForm */

use app\models\dto\RegistrationForm;
use app\models\UserType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'options'              => ['class' => 'registration-form'],
    'action'               => Url::to(['/authentication/register']),
    'enableAjaxValidation' => true,
    'validationUrl'        => Url::to(['/authentication/register-validation'])
]);

echo $form->field($model, 'first_name')->textInput();

echo $form->field($model, 'last_name')->textInput();

echo $form->field($model, 'username')->textInput();

echo $form->field($model, 'email')->textInput();

echo $form->field($model, 'password')->passwordInput();

echo $form->field($model, 'confirmPassword')->passwordInput();

echo $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(UserType::find()->all(), 'id', 'name'), [
    'prompt' => 'Select User Type'
]);

echo Html::submitButton('Register', ['class' => 'btn btn-success']);

ActiveForm::end();