<?php

/* @var $this View */
/* @var $user UserProfile */

use app\models\dto\UserProfile;
use Firebase\JWT\JWT;
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use yii\web\View;

$items = [
    'personal-information' => [
        'label' => 'Personal Information',
        'active' => Yii::$app->controller->action->id === 'personal-information'
    ],
    'security'             => [
        'label' => 'Security',
        'active' => Yii::$app->controller->action->id === 'security'
    ]
];

if ($items['personal-information']['active'] === true) {
    $items['personal-information']['content'] = $this->render('personal-information/index', compact('user'));
    $items['security']['url']                 = Url::to(['/profile/security', 'userId' => JWT::urlsafeB64Encode($user->id)]);
} else {
    $items['security']['content']             = $this->render('security/index', compact('user'));
    $items['personal-information']['url']     = Url::to(['/profile/personal-information', 'userId' => JWT::urlsafeB64Encode($user->id)]);
}

echo Tabs::widget(compact('items'));