<?php

/* @var $this View */
/* @var $user UserProfile */

use app\models\dto\UserProfile;
use Firebase\JWT\JWT;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'options' => ['class' => 'user-profile-personal-information-form'],
    'action'  => Url::to(['/profile/personal-information', 'userId' => JWT::urlsafeB64Encode($user->id)]),
    'method'  => 'POST'
]);

echo $form->field($user, 'first_name')->textInput();

echo $form->field($user, 'last_name')->textInput();

echo $form->field($user, 'username')->textInput();

echo $form->field($user, 'email')->textInput();

echo Html::submitButton('Save', ['class' => 'btn btn-success']);

ActiveForm::end();
