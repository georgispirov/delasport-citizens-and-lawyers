<?php

/* @var $this View */
/* @var $user UserProfile */

use app\models\dto\UserProfile;
use Firebase\JWT\JWT;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'id'                   => 'user-profile-security-form',
    'options'              => ['class' => 'user-profile-security-form'],
    'enableAjaxValidation' => true,
    'validationUrl'        => Url::to(['/profile/validation', 'userId' => JWT::urlsafeB64Encode($user->id)])
]);

echo $form->field($user, 'currentPassword')->passwordInput();

echo $form->field($user, 'password')->passwordInput();

echo $form->field($user, 'confirmPassword')->passwordInput();

echo Html::submitButton('Save', ['class' => 'btn btn-success']);

ActiveForm::end(); ?>

<script>
    $(function () {
        $('#user-profile-security-form').on('ajaxComplete', function (event, jqXHR) {
            let $form         = $(this);
            let $responseJSON = Object.values(jqXHR.responseJSON);

            if ($responseJSON.length) {
                $.each(jqXHR.responseJSON, function (attribute, messages) {
                    $form.yiiActiveForm('updateAttribute', attribute, messages);
                });

                let userProfileSecurityAttributes = ['userprofile-currentpassword', 'userprofile-password', 'userprofile-confirmpassword'];

                $.each(userProfileSecurityAttributes.filter(x => !Object.keys(jqXHR.responseJSON).includes(x)), function (index, diffAttribute) {
                    $form.yiiActiveForm('updateAttribute', diffAttribute, '');
                });
            } else {
                $form.yiiActiveForm('resetForm');
                $form.data('yiiActiveForm').validated = true;
            }
        });
    });
</script>
