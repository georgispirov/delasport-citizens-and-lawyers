<?php

/* @var $this View */
/* @var $searchModel UsersSearch */

use app\models\searches\UsersSearch;
use app\widgets\DelaSportDateTimePickerWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'method' => 'GET',
    'action' => Url::to(["/{$this->context->id}"])
]);

echo $form->field($searchModel, 'commonFilter')->textInput([
    'placeholder' => $searchModel->getAttributeLabel('commonFilter')
])->label(false);

if (!empty($searchModel->startCreatedDateRange) && !empty($searchModel->endCreatedDateRange)) {
    $searchModel->startCreatedDateRange = Yii::$app->getFormatter()->asDatetime($searchModel->startCreatedDateRange, 'MM/dd/Y HH:i');
    $searchModel->endCreatedDateRange   = Yii::$app->getFormatter()->asDatetime($searchModel->endCreatedDateRange, 'MM/dd/Y HH:i');
}

echo Html::button(Html::a('Reset Filters', Url::to(["/{$this->context->id}"])));

Modal::begin([
    'toggleButton' => [
        'tag'   => 'button',
        'label' => 'Additional Filters',
        'class' => 'btn btn-success'
    ]
]);

echo $form->field($searchModel, 'startCreatedDateRange')->widget(DelaSportDateTimePickerWidget::class);

echo $form->field($searchModel, 'endCreatedDateRange')->widget(DelaSportDateTimePickerWidget::class);

echo Html::submitButton('Search', ['class' => 'btn btn-success']);

Modal::end();

ActiveForm::end();
