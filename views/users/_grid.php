<?php

/* @var $this View */
/* @var $dataProvider DataProviderInterface */

use yii\data\DataProviderInterface;
use yii\grid\GridView;
use yii\web\View; ?>

<div class="row">
    <div class="col-xs-12">
        <?= GridView::widget([
            'options'      => ['class' => 'table-responsive grid-view'],
            'dataProvider' => $dataProvider,
            'columns'      => [
                'first_name',
                'last_name',
                'username',
                'email',
                'created_date',
                'updated_date',
                [
                    'attribute' => 'type.name',
                    'label'     => 'Type'
                ]
            ]
        ]); ?>
    </div>
</div>
