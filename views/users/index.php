<?php

/* @var $this View */
/* @var $searchModel UsersSearch */
/* @var $dataProvider DataProviderInterface */

use app\models\searches\UsersSearch;
use yii\data\DataProviderInterface;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\web\View;

echo Html::tag('h3', Inflector::camel2words($this->context->id));

echo $this->render('_search', compact('searchModel'));

echo $this->render('_grid', compact('dataProvider'));