<?php

/* @var $this View */
/* @var $model ApproveOrRejectAppointmentForm */

use app\models\dto\ApproveOrRejectAppointmentForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'options' => ['class' => 'approve-appointment-form'],
    'action'  => Url::to(['/lawyer-appointments/approve-or-reject', 'appointmentId' => $model->id]),
    'method'  => 'POST'
]);

$name = "{$model->formName()}[action]";

echo Html::submitButton('Approve', ['name' => $name, 'value' => 'Approve', 'class' => 'btn btn-success']);

echo Html::submitButton('Reject', ['name' => $name, 'value' => 'Reject', 'class' => 'btn btn-danger']);

ActiveForm::end();