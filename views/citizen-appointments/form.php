<?php

/* @var $this View */
/* @var $model CitizenAppointmentForm */

use app\models\AppointmentStatus;
use app\models\dto\CitizenAppointmentForm;
use app\models\Users;
use app\models\UserType;
use app\widgets\DelaSportDateTimePickerWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$activeLawyers = Users::find()->where(['type_id' => UserType::LAWYER])->all();

$form = ActiveForm::begin([
    'options'                => ['class' => 'appointment-form'],
    'enableAjaxValidation'   => true,
    'enableClientValidation' => false,
    'validationUrl'          => Url::to(['/citizen-appointments/validation'])
]);

if (!$model->isNewRecord) {
    $this->title = 'Update Appointment: ' . '<b>' . $model->name . '</b>';

    echo $form->field($model, 'id')->hiddenInput()->label(false);
    echo $form->field($model, 'status_id')->hiddenInput()->label(false);
} else {
    $this->title = 'Create Appointment';
}

$this->params['breadcrumbs'][] = $this->title;

echo $form->field($model, 'name')->textInput();

echo $form->field($model, 'description')->textarea();

if ($model->isNewRecord || $model->status_id === AppointmentStatus::STATUS_REJECTED) { ?>

    <div class="row">
        <div class="col-xs-6">
            <b>Rejected Start Date: </b>
            <span><?= $model->start_date; ?></span>
        </div>

        <div class="col-xs-6">
            <b>Rejected End Date: </b>
            <span><?= $model->end_date; ?></span>
        </div>
    </div>

    <?php $model->start_date = null;
    $model->end_date   = null;

    echo $form->field($model, 'start_date')->widget(DelaSportDateTimePickerWidget::class);

    echo $form->field($model, 'end_date')->widget(DelaSportDateTimePickerWidget::class);
} else {
    if (!$model->isNewRecord && $model->status_id !== AppointmentStatus::STATUS_REJECTED) { ?>
        <div class="row">
            <div class="col-xs-4">
                <b>Start Date: </b>
                <span><?= $model->start_date; ?></span>
            </div>

            <div class="col-xs-4">
                <b>End Date: </b>
                <span><?= $model->end_date; ?></span>
            </div>

            <div class="col-xs-4">
                <b>Status: </b>
                <span><?= $model->status->name; ?></span>
            </div>
        </div>
    <?php }
}

if ($model->isNewRecord) {
    echo $form->field($model, 'lawyerId')->dropDownList(ArrayHelper::map($activeLawyers, 'id', function (Users $lawyer) {
        return $lawyer->first_name . ' ' . $lawyer->last_name;
    }), ['prompt' => 'Choose Lawyer']);
} else {
    echo $form->field($model, 'lawyerId')->textInput(['disabled' => 'disabled', 'value' => $model->userAppointment->lawyer->first_name . ' ' . $model->userAppointment->lawyer->last_name]);
}

$submitButtonTitle = $model->isNewRecord ? 'Send Appointment' : 'Update Appointment';

echo Html::submitButton($submitButtonTitle, ['class' => 'btn btn-success']);

ActiveForm::end(); ?>

<script>
    $(function () {
        let $form = $('.appointment-form');

        $form.on('ajaxComplete', function(event, jqXHR) {
            $form.yiiActiveForm('updateMessages', jqXHR.responseJSON, true);
            $form.yiiActiveForm('data').validated = Object.values(jqXHR.responseJSON).filter(x => x.length !== 0).length ? false : true;
        });
    });
</script>
