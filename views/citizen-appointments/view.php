<?php

/* @var $this View */
/* @var $model ApproveOrRejectAppointmentForm */

use app\models\AppointmentStatus;
use app\models\dto\ApproveOrRejectAppointmentForm;
use yii\helpers\Html;
use yii\web\Response;
use yii\web\View;
use yii\widgets\DetailView;

echo Html::tag('h3', 'Appointment Request');

echo DetailView::widget([
    'model'      => $model,
    'attributes' => [
        'name',
        'description',
        [
            'attribute' => 'citizen',
            'label'     => 'Sent From',
            'value'     => function (ApproveOrRejectAppointmentForm $appointment) {
                $citizen = $appointment->userAppointment->citizen;
                return $citizen->first_name . ' ' . $citizen->last_name;
            }
        ],
        [
            'attribute' => 'lawyer',
            'label'     => 'Sent To',
            'value'     => function (ApproveOrRejectAppointmentForm $appointment) {
                $lawyer = $appointment->userAppointment->lawyer;
                return 'You (' . $lawyer->first_name . ' ' . $lawyer->last_name . ')';
            }
        ],
        [
            'attribute' => 'created_date',
            'format'    => Response::FORMAT_RAW
        ],
        [
            'attribute' => 'updated_date',
            'format'    => Response::FORMAT_RAW
        ],
        [
            'attribute' => 'start_date',
            'format'    => Response::FORMAT_RAW
        ],
        [
            'attribute' => 'end_date',
            'format'    => Response::FORMAT_RAW
        ],
        [
            'attribute' => 'status.name',
            'label'     => 'Status'
        ]
    ]
]);

if (Yii::$app->user->identity->getIsLawyer()
    && array_search($model->status_id, [AppointmentStatus::STATUS_PENDING_REVIEW, AppointmentStatus::STATUS_RECEIVED, AppointmentStatus::STATUS_RESCHEDULED])
    && Yii::$app->user->can('approveAppointment', ['appointmentId' => $model->id]))
{
    echo $this->render('_approve_appointment', compact('model'));
}