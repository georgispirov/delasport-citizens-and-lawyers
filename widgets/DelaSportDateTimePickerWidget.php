<?php

namespace app\widgets;

use yii\helpers\Html;
use yii\widgets\InputWidget;

class DelaSportDateTimePickerWidget extends InputWidget
{
    /**
     * @return string
     */
    public function run()
    {
        return <<<HTML
        <div class="datepicker-container">
            <div class="form-group">
                <div class="input-group date">
                    {$this->renderDateTimePickerInputField()}
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
HTML;
    }

    /**
     * @return string
     */
    private function renderDateTimePickerInputField(): string
    {
        return Html::activeInput('text', $this->model, $this->attribute, ['class' => 'form-control dela-sport-date-picker-widget']);
    }
}