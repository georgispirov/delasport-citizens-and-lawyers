<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_appointments".
 *
 * @property int $citizen_id
 * @property int $lawyer_id
 * @property int $appointment_id
 *
 * @property Appointments $appointment
 * @property Users $citizen
 * @property Users $lawyer
 */
class UserAppointments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_appointments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['citizen_id', 'lawyer_id', 'appointment_id'], 'required'],
            [['citizen_id', 'lawyer_id', 'appointment_id'], 'default', 'value' => null],
            [['citizen_id', 'lawyer_id', 'appointment_id'], 'integer'],
            [['citizen_id', 'lawyer_id', 'appointment_id'], 'unique', 'targetAttribute' => ['citizen_id', 'lawyer_id', 'appointment_id']],
            [['appointment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Appointments::class, 'targetAttribute' => ['appointment_id' => 'id']],
            [['citizen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['citizen_id' => 'id']],
            [['lawyer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['lawyer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'citizen_id' => 'Citizen ID',
            'lawyer_id' => 'Lawyer ID',
            'appointment_id' => 'Appointment ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppointment()
    {
        return $this->hasOne(Appointments::class, ['id' => 'appointment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCitizen()
    {
        return $this->hasOne(Users::class, ['id' => 'citizen_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLawyer()
    {
        return $this->hasOne(Users::class, ['id' => 'lawyer_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserAppointmentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserAppointmentsQuery(get_called_class());
    }
}
