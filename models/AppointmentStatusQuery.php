<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AppointmentStatus]].
 *
 * @see AppointmentStatus
 */
class AppointmentStatusQuery extends \yii\db\ActiveQuery
{
    public function all($db = null)
    {
        return parent::all($db);
    }

    public function one($db = null)
    {
        return parent::one($db);
    }
}
