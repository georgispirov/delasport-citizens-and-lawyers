<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "appointments".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $created_date
 * @property string $updated_date
 * @property int $status_id
 * @property string $start_date
 * @property string $end_date
 *
 * @property AppointmentStatus $status
 * @property UserAppointments $userAppointment
 */
class Appointments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'appointments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['created_date', 'updated_date', 'start_date', 'end_date'], 'safe'],
            [['status_id'], 'default', 'value' => null],
            [['status_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => AppointmentStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    public function behaviors(): array
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'created_date',
                'updatedAtAttribute' => 'updated_date',
                'value'              => new Expression('now()')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'name'         => 'Name',
            'description'  => 'Description',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
            'start_date'   => 'Start Date',
            'end_date'     => 'End Date',
            'status_id'    => 'Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(AppointmentStatus::class, ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAppointment()
    {
        return $this->hasOne(UserAppointments::class, ['appointment_id' => 'id']);
    }

    public function afterFind()
    {
        if (!empty($this->start_date)) {
            $this->start_date   = Yii::$app->getFormatter()->asDatetime($this->start_date, 'MM/dd/Y HH:i');
        }

        if (!empty($this->end_date)) {
            $this->end_date     = Yii::$app->getFormatter()->asDatetime($this->end_date, 'MM/dd/Y HH:i');
        }

        if (!empty($this->created_date)) {
            $this->created_date = Yii::$app->getFormatter()->asDatetime($this->created_date, 'MM/dd/Y HH:i');
        }

        if (!empty($this->updated_date)) {
            $this->updated_date = Yii::$app->getFormatter()->asDatetime($this->updated_date, 'MM/dd/Y HH:i');
        }

        parent::afterFind();
    }

    /**
     * {@inheritdoc}
     * @return AppointmentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AppointmentsQuery(get_called_class());
    }
}
