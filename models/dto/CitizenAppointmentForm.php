<?php

namespace app\models\dto;

use app\models\Appointments;

class CitizenAppointmentForm extends Appointments
{
    const SCENARIO_VALIDATION = 'scenario-validation';

    /**
     * @var mixed $lawyerId
     */
    public $lawyerId;

    /**
     * @return array
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['name', 'description', 'start_date', 'end_date', 'lawyerId'], 'safe'],
            [['name', 'description', 'start_date', 'end_date', 'lawyerId'], 'required'],
            ['end_date', 'appointmentDatesValidator']
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'lawyerId' => 'Lawyer'
        ]);
    }

    /**
     * @return array
     */
    public function scenarios(): array
    {
        $parentScenarios = parent::scenarios();
        $parentScenarios[static::SCENARIO_VALIDATION] = ['name', 'description', 'start_date', 'end_date', 'lawyerId', 'id', 'status_id'];
        return $parentScenarios;
    }

    public function appointmentDatesValidator($attribute)
    {
        if (strtotime($this->{$attribute}) <= strtotime($this->start_date)) {
            $this->addError($attribute, 'End date should be greater than start date.');
        }
    }

    public function afterFind()
    {
        $this->lawyerId = $this->userAppointment->lawyer_id;
        parent::afterFind();
    }
}