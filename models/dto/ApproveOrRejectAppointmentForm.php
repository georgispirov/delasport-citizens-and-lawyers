<?php

namespace app\models\dto;

use app\models\Appointments;

class ApproveOrRejectAppointmentForm extends Appointments
{
    /**
     * @var bool $action
     */
    public $action;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['action'], 'safe']
        ];
    }
}