<?php

namespace app\models\dto;

use app\models\Users;

class UserProfile extends Users
{
    const SCENARIO_PERSONAL_INFORMATION = 'scenario-personal-information';
    const SCENARIO_SECURITY             = 'scenario-security';

    /**
     * @var mixed $currentPassword
     */
    public $currentPassword;

    /**
     * @var mixed $confirmPassword
     */
    public $confirmPassword;

    /**
     * @return array
     */
    public function scenarios(): array
    {
        $parentScenarios = parent::scenarios();

        $parentScenarios[static::SCENARIO_PERSONAL_INFORMATION] = ['first_name', 'last_name', 'username', 'email'];
        $parentScenarios[static::SCENARIO_SECURITY]             = ['currentPassword', 'password', 'confirmPassword'];

        return $parentScenarios;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['currentPassword', 'password', 'confirmPassword'], 'required', 'on' => static::SCENARIO_SECURITY],
            [['first_name', 'last_name', 'username', 'email'], 'required', 'on' => static::SCENARIO_PERSONAL_INFORMATION],
            ['password', 'match', 'pattern' => '/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/', 'message' => 'Password should contains at least one capital letter and one digit.'],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function attributes(): array
    {
        return array_merge(parent::attributes(), [
            'currentPassword',
            'confirmPassword'
        ]);
    }
}