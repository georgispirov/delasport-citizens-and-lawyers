<?php

namespace app\models\dto;

use app\models\Users;

class LoginForm extends Users
{
    /**
     * @var mixed $identifier
     */
    public $identifier;

    /**
     * @return array
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['identifier', 'password'], 'safe'],
            [['identifier', 'password'], 'required']
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'identifier' => 'Username / Email'
        ]);
    }
}