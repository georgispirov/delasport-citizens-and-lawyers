<?php

namespace app\models\dto;

use app\models\Users;

class RegistrationForm extends Users
{
    /**
     * @var mixed $confirmPassword
     */
    public $confirmPassword;

    /**
     * @return array
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['username', 'password', 'email', 'confirmPassword', 'type_id'], 'safe'],
            ['email', 'email'],
            ['password', 'match', 'pattern' => '/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/', 'message' => 'Password should contains at least one capital letter and one digit.'],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password'],
            [['first_name', 'last_name', 'username', 'email', 'password', 'confirmPassword', 'type_id'], 'required'],
            [['email', 'username'], 'validateUniqueness']
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'type_id' => 'User Type'
        ]);
    }

    /**
     * @param $attribute
     */
    public function validateUniqueness($attribute)
    {
        if (Users::find()->where([$attribute => $this->{$attribute}])->exists()) {
            $this->addError($attribute,  "User with this {$this->getAttributeLabel($attribute)} already exists.");
        }
    }
}