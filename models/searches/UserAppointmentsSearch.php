<?php

namespace app\models\searches;

use app\behaviors\PostgresTimestampDateParserAfterValidateBehavior;
use app\models\AppointmentsQuery;
use app\models\UserAppointments;
use app\models\Users;
use app\models\UsersQuery;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\conditions\LikeCondition;

class UserAppointmentsSearch extends UserAppointments
{
    /**
     * @var string $commonFilter
     */
    public $commonFilter;

    /**
     * @var string $startDateRange
     */
    public $startDateRange;

    /**
     * @var string $endDateRange
     */
    public $endDateRange;

    /**
     * @var string $startCreatedDateRange
     */
    public $startCreatedDateRange;

    /**
     * @var string $endCreatedDateRange
     */
    public $endCreatedDateRange;

    /**
     * @var string $startUpdatedDateRange
     */
    public $startUpdatedDateRange;

    /**
     * @var string $endUpdatedDateRange
     */
    public $endUpdatedDateRange;

    /**
     * @var mixed $appointmentStatusId
     */
    public $appointmentStatusId;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [
                ['commonFilter', 'startDateRange', 'endDateRange', 'startCreatedDateRange', 'endCreatedDateRange', 'startUpdatedDateRange', 'endUpdatedDateRange', 'appointmentStatusId'], 'safe'
            ],
            [
                'endCreatedDateRange',
                'compare',
                'compareAttribute' => 'startCreatedDateRange',
                'operator'         => '>',
                'message'          => 'Created end date should be greater than start updated date.'
            ],
            [
                'endDateRange',
                'compare',
                'compareAttribute' => 'startDateRange',
                'operator'         => '>',
                'message'          => 'Start end date should be greater than start updated date.'
            ],
            [
                'endUpdatedDateRange',
                'compare',
                'compareAttribute' => 'startUpdatedDateRange',
                'operator'         => '>',
                'message'          => 'Updated end date should be greater than start updated date.'
            ]
        ];
    }

    public function behaviors(): array
    {
        return [
            [
                'class'          => PostgresTimestampDateParserAfterValidateBehavior::class,
                'dateAttributes' => [
                    'startDateRange', 'endDateRange', 'startCreatedDateRange', 'endCreatedDateRange', 'startUpdatedDateRange', 'endUpdatedDateRange'
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'commonFilter'        => 'Search by Citizen, Lawyer, Name, Description...',
            'appointmentStatusId' => 'Status'
        ]);
    }

    /**
     * @param array $params
     * @return DataProviderInterface
     */
    public function search(array $params): DataProviderInterface
    {
        $query = static::find();

        $this->load($params);

        if (!$this->validate()) {
            $query->emulateExecution();
        }

        $query->joinWith([
            'lawyer'  => function (UsersQuery $lawyerQuery) {
                $lawyerQuery->select(['lawyer.id', 'lawyer.first_name', 'lawyer.last_name']);
                $lawyerQuery->from(['lawyer' => Users::tableName()]);
                return $lawyerQuery;
            },
            'citizen' => function (UsersQuery $citizenQuery) {
                $citizenQuery->select(['citizen.id', 'citizen.first_name', 'citizen.last_name']);
                $citizenQuery->from(['citizen' => Users::tableName()]);
                return $citizenQuery;
            },
            'appointment' => function (AppointmentsQuery $appointmentQuery) {
                $appointmentQuery->joinWith(['status']);
                return $appointmentQuery;
            }
        ]);

        if (!empty($this->commonFilter)) {
            $query->andWhere([
                'or',
                new LikeCondition("concat_ws('', citizen.first_name, citizen.last_name)", 'ILIKE', $this->commonFilter),
                new LikeCondition("concat_ws('', lawyer.first_name, lawyer.last_name)",   'ILIKE', $this->commonFilter),
                new LikeCondition('appointments.name',                                    'ILIKE', $this->commonFilter),
                new LikeCondition('appointments.description',                             'ILIKE', $this->commonFilter)
            ]);
        }

        if (!empty($this->appointmentStatusId)) {
            $query->andWhere(['appointments.status_id' => $this->appointmentStatusId]);
        }

        if (!empty($this->startDateRange) && !empty($this->endDateRange)) {
            $query->andWhere([
                'and',
                ['>=', 'appointments.start_date', $this->startDateRange],
                ['<=', 'appointments.end_date', $this->endDateRange],
            ]);
        }

        if (!empty($this->startCreatedDateRange) && !empty($this->endCreatedDateRange)) {
            $query->andWhere([
                'and',
                ['>=', 'appointments.created_date', $this->startCreatedDateRange],
                ['<=', 'appointments.created_date', $this->endCreatedDateRange],
            ]);
        }

        if (!empty($this->startUpdatedDateRange) && !empty($this->endUpdatedDateRange)) {
            $query->andWhere([
                'and',
                ['>=', 'appointments.updated_date', $this->startUpdatedDateRange],
                ['<=', 'appointments.updated_date', $this->endUpdatedDateRange],
            ]);
        }

        if (Yii::$app->getUser()->identity->getIsCitizen()) {
            $query->andWhere(['citizen_id' => Yii::$app->getUser()->getId()]);
        } else {
            $query->andWhere(['lawyer_id'  => Yii::$app->getUser()->getId()]);
        }

        $sort = $this->prepareDataProviderSorter();

        return new ActiveDataProvider(compact('query', 'sort'));
    }

    /**
     * @return Sort
     */
    private function prepareDataProviderSorter(): Sort
    {
        return new Sort([
            'attributes' => [
                'citizen' => [
                    'asc'  => ['citizen.first_name' => SORT_ASC, 'citizen.last_name'  => SORT_ASC],
                    'desc' => ['citizen.first_name' => SORT_DESC, 'citizen.last_name' => SORT_DESC]
                ],
                'lawyer' => [
                    'asc'  => ['lawyer.first_name' => SORT_ASC, 'lawyer.last_name'  => SORT_ASC],
                    'desc' => ['lawyer.first_name' => SORT_DESC, 'lawyer.last_name' => SORT_DESC]
                ],
                'appointment.name' => [
                    'asc'  => ['appointments.name' => SORT_ASC],
                    'desc' => ['appointments.name' => SORT_DESC]
                ],
                'appointment.description' => [
                    'asc'  => ['appointments.description' => SORT_ASC],
                    'desc' => ['appointments.description' => SORT_DESC]
                ],
                'appointment.status.name' => [
                    'asc'  => ['appointment_status.name' => SORT_ASC],
                    'desc' => ['appointment_status.name' => SORT_DESC]
                ],
                'appointment.start_date' => [
                    'asc'  => ['appointments.start_date' => SORT_ASC],
                    'desc' => ['appointments.start_date' => SORT_DESC]
                ],
                'appointment.end_date' => [
                    'asc'  => ['appointments.end_date' => SORT_ASC],
                    'desc' => ['appointments.end_date' => SORT_DESC]
                ],
                'appointment.created_date' => [
                    'asc'  => ['appointments.created_date' => SORT_ASC],
                    'desc' => ['appointments.created_date' => SORT_DESC]
                ],
                'appointment.updated_date' => [
                    'asc'  => ['appointments.updated_date' => SORT_ASC],
                    'desc' => ['appointments.updated_date' => SORT_DESC]
                ]
            ]
        ]);
    }
}