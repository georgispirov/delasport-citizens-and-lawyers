<?php

namespace app\models\searches;

use app\behaviors\PostgresTimestampDateParserAfterValidateBehavior;
use app\models\Users;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\conditions\LikeCondition;

class UsersSearch extends Users
{
    /**
     * @var string $commonFilter
     */
    public $commonFilter;

    /**
     * @var string $startCreatedDateRange
     */
    public $startCreatedDateRange;

    /**
     * @var string $endCreatedDateRange
     */
    public $endCreatedDateRange;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['commonFilter', 'startCreatedDateRange', 'endCreatedDateRange'], 'safe'],
            [
                'endCreatedDateRange',
                'compare',
                'compareAttribute' => 'startCreatedDateRange',
                'operator'         => '>',
                'message'          => 'End date should be greater than start date.'
            ]
        ];
    }

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
            [
                'class'          => PostgresTimestampDateParserAfterValidateBehavior::class,
                'dateAttributes' => ['startCreatedDateRange', 'endCreatedDateRange']
            ]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'commonFilter'          => 'Search by First name, Last Name, Username, Email...',
            'startCreatedDateRange' => 'Start Created Date',
            'endCreatedDateRange'   => 'End Created Date',
        ]);
    }

    /**
     * @param array $params
     * @param int $userType
     * @return DataProviderInterface
     * @throws \yii\base\InvalidConfigException
     */
    public function search(array $params, int $userType): DataProviderInterface
    {
        $query = static::find();

        $this->load($params);

        if (isset($_GET[$this->formName()]) && !$this->validate()) {
            $query->emulateExecution();
        }

        $query->select([
            'users.id', 'users.first_name', 'users.last_name', 'users.username', 'users.email',
            'users.created_date', 'users.updated_date', 'users.type_id'
        ]);

        $query->joinWith(['type']);

        if (!empty($this->commonFilter)) {
            $query->andWhere([
                'or',
                new LikeCondition("concat_ws(' ', users.first_name, users.last_name)", 'ILIKE', $this->commonFilter),
                new LikeCondition('users.username',                                    'ILIKE', $this->commonFilter),
                new LikeCondition('users.email',                                       'ILIKE', $this->commonFilter)
            ]);
        }

        if (!empty($this->startCreatedDateRange) && !empty($this->endCreatedDateRange)) {
            $query->andWhere([
                'and',
                'users.created_date::abstime::int >= ' . strtotime($this->startCreatedDateRange),
                'users.created_date::abstime::int <= ' . strtotime($this->endCreatedDateRange),
            ]);
        }

        $query->andWhere(['users.type_id' => $userType]);

        return new ActiveDataProvider(compact('query'));
    }
}