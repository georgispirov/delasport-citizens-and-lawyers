<?php

namespace app\models;

use app\interfaces\UserTypeNegotiatorInterface;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $created_date
 * @property string $updated_date
 * @property int $type_id
 *
 * @property UserType $type
 */
class Users extends ActiveRecord implements IdentityInterface, UserTypeNegotiatorInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    public function behaviors(): array
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'created_date',
                'updatedAtAttribute' => 'updated_date',
                'value'              => new Expression('NOW()')
            ]
        ];
    }

    public function afterFind()
    {
        if (!empty($this->created_date)) {
            $this->created_date = Yii::$app->getFormatter()->asDatetime($this->created_date, 'MM/dd/Y HH:i');
        }

        if (!empty($this->updated_date)) {
            $this->updated_date = Yii::$app->getFormatter()->asDatetime($this->updated_date, 'MM/dd/Y HH:i');
        }

        parent::afterFind();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_date', 'updated_date'], 'safe'],
            [['type_id'], 'default', 'value' => null],
            [['type_id'], 'integer'],
            [['first_name', 'last_name'], 'string', 'max' => 155],
            [['username'], 'string', 'max' => 200],
            [['email', 'password'], 'string', 'max' => 255],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserType::class, 'targetAttribute' => ['type_id' => 'id']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    /**
     * {@inheritdoc}
     * @return UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersQuery(get_called_class());
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return void|IdentityInterfaceAlias|null
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('Not supported access token.');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return null;
    }

    public function validateAuthKey($authKey)
    {
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(UserType::class, ['id' => 'type_id']);
    }

    /**
     * @return bool
     */
    public function getIsCitizen(): bool
    {
        return $this->type_id === UserType::CITIZEN;
    }

    /**
     * @return bool
     */
    public function getIsLawyer(): bool
    {
        return $this->type_id === UserType::LAWYER;
    }
}
