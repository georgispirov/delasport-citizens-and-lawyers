<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "appointment_status".
 *
 * @property int $id
 * @property string $name
 *
 * @property Appointments[] $appointments
 */
class AppointmentStatus extends \yii\db\ActiveRecord
{
    const STATUS_PENDING_REVIEW = 1;
    const STATUS_APPROVED       = 2;
    const STATUS_REJECTED       = 3;
    const STATUS_RECEIVED       = 4;
    const STATUS_RESCHEDULED    = 5;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'appointment_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppointments()
    {
        return $this->hasMany(Appointments::class, ['status_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return AppointmentStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AppointmentStatusQuery(get_called_class());
    }
}
