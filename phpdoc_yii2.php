<?php

use app\components\GoogleAPI;
use app\models\Users;
use yii\base\Component;

/**
 * @property User $user The user component. This property is read-only.
 * @property GoogleAPI $googleApis
 */
abstract class Application extends \yii\web\Application
{
    /**
     * @return \yii\web\User|User
     */
    public function getUser()
    {
        return parent::getUser();
    }
}

/**
 * Class Yii
 */
class Yii extends \yii\BaseYii
{
    /**
     * @var Application
     */
    public static $app;
}

/**
 * Class User
 *
 * @property null|Users $identity The identity object associated with the currently logged-in
 */
class User extends Component
{

}
