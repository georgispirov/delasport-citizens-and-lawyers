<?php

use yii\db\Connection;

$dbConfig = require_once __DIR__ . '/db-config.php';

return [
    'class'    => Connection::class,
    'dsn'      => "pgsql:host=localhost;dbname={$dbConfig['name']}",
    'username' => $dbConfig['user'],
    'password' => $dbConfig['password'],
    'charset'  => 'utf8',
];
