<?php

namespace app\traits;

use app\models\UserAppointments;
use Yii;

trait CommonUserAppointmentAuthRuleCheckerTrait
{
    /**
     * @param $userId
     * @param $params
     * @return bool
     */
    public function commonAccessChecker($userId, $params): bool
    {
        $userTypeColumn = Yii::$app->getUser()->identity->getIsCitizen() ? 'citizen_id' : 'lawyer_id';
        $query          = UserAppointments::find()->where(['appointment_id' => $params['appointmentId'], $userTypeColumn => $userId]);
        return $query->exists();
    }
}