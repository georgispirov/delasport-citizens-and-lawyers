<?php

namespace app\traits;

use app\models\UserAppointments;

trait CommonLawyersAuthRuleCheckerTrait
{
    /**
     * @param $user
     * @param $params
     * @return bool
     */
    public function commonLawyerAccessCheck($user, $params)
    {
        $query = UserAppointments::find()->where(['appointment_id' => $params['appointmentId'], 'lawyer_id' => $user]);
        return $query->exists();
    }
}